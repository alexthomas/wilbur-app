FROM node:20
COPY build /app
COPY package.json /app
WORKDIR /app
ENV PORT=3001
EXPOSE 3001
CMD ["node", "index.js"]

