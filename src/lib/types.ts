export type Message = {
    role: string,
    content: string,
    function_call?: string
    parameters?: any
};

export type Conversation = {
    id: string,
    startedDate: string,
    messages: Message[],
}

export type Equipment = {
    initial: string,
    number: string,
    waybillSerialNumber: string,
    shipmentStatus: string,
    originCity: string,
    originState: string,
    destinationCity: string,
    destinationState: string,
    eta: string,
    lastEventTime: string,
    lastEvent: string,
    lastEventDescription: string,
    lastEventCity: string,
    lastEventState: string,
}
